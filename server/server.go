package server

import (
	"bankapi/model"
	"bankapi/service"

	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Server struct {
	UserService        service.UserService
	BankAccountService service.BankAccountService
}

func (s *Server) CreateUser(c *gin.Context) {
	var user model.User
	err := c.ShouldBindJSON(&user)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"object":  "error",
			"message": fmt.Sprintf("json: wrong params: %s", err),
		})
		return
	}

	if err := s.UserService.Create(&user); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusCreated, user)
}

func (s *Server) GetAllUsers(c *gin.Context) {
	users, err := s.UserService.All()

	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusCreated, users)
}

func (s *Server) GetUserById(c *gin.Context) {
	id := c.Param("id")
	user, err := s.UserService.GetById(id)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, user)
}

func (s *Server) GetBankAccounts(c *gin.Context) {

}
