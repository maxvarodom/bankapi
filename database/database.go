package database

import (
	"bankapi/model"

	"firebase.google.com/go/db"

	firebase "firebase.google.com/go"
	"golang.org/x/net/context"

	"log"

	"google.golang.org/api/option"
)

type DB struct {
	Context context.Context
	Client  *db.Client
}

func SetupFirebase() (context.Context, *db.Client) {
	ctx := context.Background()
	conf := &firebase.Config{
		DatabaseURL: "https://bankapi-6be4a.firebaseio.com",
	}

	opt := option.WithCredentialsFile("bankapi-6be4a-c5996be0f4d5.json")

	app, err := firebase.NewApp(ctx, conf, opt)
	if err != nil {
		log.Fatalln("Error initializing app:", err)
	}

	client, err := app.Database(ctx)
	if err != nil {
		log.Fatalln("Error initializing database client:", err)
	}

	return ctx, client
}

func (db *DB) InsertUser(u *model.User) error {
	var err error
	var users []model.User
	ref := db.Client.NewRef("/users")
	err = ref.Get(db.Context, &users)
	users = append(users, *u)
	err = ref.Set(db.Context, users)
	return err
}

func (db *DB) GetAllUser() ([]model.User, error) {
	ref := db.Client.NewRef("/users")
	var users []model.User
	err := ref.Get(db.Context, &users)
	return users, err
}

func (db *DB) FindUserById(id string) (*model.User, error) {
	var user model.User
	ref := db.Client.NewRef("/users")
	mapUser := model.User{
		Id: id,
	}
	err := ref.OrderByKey().EqualTo(mapUser).Get(db.Context, &user)
	return &user, err
}
