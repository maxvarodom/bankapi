package router

import (
	"bankapi/server"

	"github.com/gin-gonic/gin"
)

func Setup(s *server.Server) *gin.Engine {
	router := gin.Default()
	setupRouterUsers(s, router)
	setupRouterBankAccounts(s, router)
	return router
}

func setupRouterUsers(s *server.Server, g *gin.Engine) {
	users := g.Group("/users")
	users.GET("/", s.GetAllUsers)
	users.POST("/", s.CreateUser)
	users.GET("/:id", s.GetUserById)
}

func setupRouterBankAccounts(s *server.Server, g *gin.Engine) {
	bankAccounts := g.Group("/admin")
	bankAccounts.GET("/", s.GetBankAccounts)
}
