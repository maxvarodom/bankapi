package model

type User struct {
	Id           string        `json:"id"`
	First_Name   string        `json:"first_name"`
	Last_Name    string        `json:"last_name"`
	BackAccounts []BankAccount `json:"bank_accounts"`
}

type BankAccount struct {
	Id             string  `json:"id"`
	User_Id        string  `json:"user_id"`
	Account_Number string  `json:"account_number"`
	Name           string  `json:"name"`
	Balance        float64 `json:"balance"`
}
