module bankapi

require (
	cloud.google.com/go v0.33.1 // indirect
	firebase.google.com/go v3.4.0+incompatible
	github.com/gin-contrib/sse v0.0.0-20170109093832-22d885f9ecc7 // indirect
	github.com/gin-gonic/gin v1.3.0
	github.com/google/uuid v1.1.0
	github.com/googleapis/gax-go v2.0.2+incompatible // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/ugorji/go/codec v0.0.0-20181127175209-856da096dbdf // indirect
	go.opencensus.io v0.18.0 // indirect
	golang.org/x/net v0.0.0-20181129055619-fae4c4e3ad76
	golang.org/x/oauth2 v0.0.0-20181128211412-28207608b838 // indirect
	google.golang.org/api v0.0.0-20181129220737-af4fc4062c26
	google.golang.org/genproto v0.0.0-20181127195345-31ac5d88444a // indirect
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
