package service

import (
	"bankapi/database"
	"bankapi/model"

	"github.com/google/uuid"
)

type UserService interface {
	All() (*[]model.User, error)
	Create(user *model.User) error
	GetById(id string) (*model.User, error)
}

type UserServiceImprement struct {
	DB database.DB
}

func (u *UserServiceImprement) All() (*[]model.User, error) {
	users, err := u.DB.GetAllUser()
	return &users, err
}

func (u *UserServiceImprement) Create(user *model.User) error {
	id := uuid.New().String()
	user.Id = id
	err := u.DB.InsertUser(user)
	return err
}

func (u *UserServiceImprement) GetById(id string) (*model.User, error) {
	users, err := u.DB.GetAllUser()

	for _, user := range users {
		if id == user.Id {
			return &user, nil
		}
	}

	// user, err := u.DB.FindUserById(id)

	return nil, err
}
