package service

import (
	"bankapi/model"
)

type BankAccountService interface {
	Get(id string) (*model.BankAccount, error)
}

type BankAccountServiceImprement struct {
}

func (u *BankAccountServiceImprement) Get(id string) (*model.BankAccount, error) {

	return nil, nil
}
