package main

import (
	"bankapi/database"
	"bankapi/router"
	"bankapi/server"
	"bankapi/service"
	"os"
)

func main() {
	context, client := database.SetupFirebase()	
	s := &server.Server{
		UserService: &service.UserServiceImprement{
			DB: database.DB{
				Context: context,
				Client: client,
			},
		},
		BankAccountService: &service.BankAccountServiceImprement{},
	}
	r := router.Setup(s)
	r.Run(":" + os.Getenv("PORT"))
}
